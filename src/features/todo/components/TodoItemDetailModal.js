import { Modal } from "antd";
import { useTodos } from "../../hooks/useTodo";
import { useEffect, useState } from "react";
import * as todoApi from "../../../apis/todo"

export default function TodoItemDetailModal(props) {

    const { taskId, isDetailModalOpen, setIsDetailModalOpen } = props
    const { getTodoById } = useTodos()
    const [task, setTask] = useState()
    useEffect(() => {
        const getTask = async () => {
            const response = await todoApi.getTodoTaskById(taskId)
            setTask(response.data)
         }
         getTask()
    }, [])
    const handleOk = () => {
        setIsDetailModalOpen(false)
    }

    const handleCancel = () => {
        setIsDetailModalOpen(false)
    }
    return (
        <Modal title="Change Task Name" open={isDetailModalOpen} onOk={handleOk} onCancel={handleCancel} >
            <p>Id: {task?.id}</p>
            <p>Name: {task?.name}</p>
            <p>Done: {task?.done.toString()}</p>
        </Modal>
    )
}